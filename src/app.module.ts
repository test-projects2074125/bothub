import { Module } from "@nestjs/common";
// Modules
import { AccountModule, LibraryModule } from "@bothub/infrastructure/modules";
import { LoggerModule } from "@bothub/infrastructure/services/logger/logger.module";

@Module({
  imports: [LoggerModule, AccountModule, LibraryModule],
})
export class AppModule {}
