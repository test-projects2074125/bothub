import { Module } from "@nestjs/common";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { MailService } from "@bothub/infrastructure/services/sms-sender/mail.service";

@Module({
  providers: [LoggerService, MailService],
  exports: [MailService],
})
export class SmsSenderModule {}
