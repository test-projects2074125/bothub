import { applyDecorators, UseGuards } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
// Guards
import { JwtAuthGuard, RolesGuard } from "@bothub/infrastructure/common";
// Types nad Enums
import { UserRole } from "@bothub/core/constants/common.enums";

export const AcceptedRoles = Reflector.createDecorator<boolean[]>();

export const UseJwtAuthWithRoleGuards = (roles: boolean[]) =>
  applyDecorators(UseGuards(JwtAuthGuard), UseGuards(RolesGuard), AcceptedRoles(roles));

const GuardRoles: { Admin: boolean[]; User: boolean[] } = {
  Admin: [UserRole.Admin],
  User: [UserRole.Admin, UserRole.User],
};

export const UserPermissions = {
  account: {
    user: {
      changeRole: GuardRoles.Admin,
    },
  },
  library: {
    books: {
      add: GuardRoles.Admin,
      update: GuardRoles.Admin,
      delete: GuardRoles.Admin,
    },
  },
};
