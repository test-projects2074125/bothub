import { Constructor } from "@nestjs/common/utils/merge-with-values.util";
import { Model } from "sequelize-typescript";

export interface ResponseFormat<T> {
  path: string;
  duration: string;
  method: string;
  isArray: boolean;
  data: T;
}

export interface UseCase<T, A = any> {
  execute(args: A): T | Promise<T>;
}

export type ModelType<T extends Model> = Constructor<T> & typeof Model;

export interface ListItemsPresenterData<T> {
  rows: T[];
  total: number;
}
