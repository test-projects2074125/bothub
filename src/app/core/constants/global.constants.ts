export class GlobalConstants {
  // Security constants
  public static readonly DEFAULT_REQUEST_TIMEOUT_MS = 5000;

  // Library
  public static readonly PUBLICATION_DATE_REGEXP: RegExp = /^(\d{1,2})\.(\d{1,2})\.(\d{2,4})$/;
}
