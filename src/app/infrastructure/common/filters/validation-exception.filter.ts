import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
  UnprocessableEntityException,
} from "@nestjs/common";
// Utils
import * as _ from "lodash";
import { HttpExceptionUtil } from "@bothub/infrastructure/utils";
import { ExceptionData } from "@bothub/core/classes/exception-data.class";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
// Types
import { Response } from "express";
import { ExceptionFilterResponse } from "@bothub/interfaces";

@Catch(UnprocessableEntityException)
export class ValidationExceptionFilter implements ExceptionFilter {
  private readonly httpExceptionUtil: HttpExceptionUtil;

  constructor(logger: LoggerService) {
    this.httpExceptionUtil = new HttpExceptionUtil(logger);
  }

  public catch(exception: HttpException | Error, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse() as Response<ExceptionFilterResponse>;
    const request: any = ctx.getRequest();

    const status = HttpStatus.UNPROCESSABLE_ENTITY;
    let exceptionData: ExceptionData;

    if (exception instanceof UnprocessableEntityException) {
      const exceptionResponse = exception.getResponse();

      if (
        _.isObject(exceptionResponse) &&
        "message" in exceptionResponse &&
        _.isObject(exceptionResponse["message"])
      ) {
        exceptionData = new ExceptionData({
          message: "Validation error",
          data: exceptionResponse.message,
        });
      } else {
        exceptionData = this.httpExceptionUtil.formatExceptionData(exception);
      }
    } else {
      exceptionData = this.httpExceptionUtil.formatExceptionData(exception);
    }
    this.httpExceptionUtil.logMessage(request, exceptionData, status);

    return response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
      error: exceptionData.buildResponse(),
    });
  }
}
