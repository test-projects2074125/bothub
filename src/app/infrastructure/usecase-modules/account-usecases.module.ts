import { DynamicModule, Module } from "@nestjs/common";
// Modules and Services
import { LoggerModule } from "@bothub/infrastructure/services/logger/logger.module";
import { ExceptionsModule } from "@bothub/infrastructure/services/exception/exception.module";
import { SmsSenderModule } from "@bothub/infrastructure/services/sms-sender/sms-sender.module";
import { DataServicesModule } from "@bothub/infrastructure/services/data-services/data-services.module";
import { JwtService } from "@nestjs/jwt";
// Use cases
import {
  AuthUseCase,
  ChangeUserRoleUseCase,
  GetCurrentUserDataUseCase,
  SignupUseCase,
} from "@bothub/infrastructure/usecases";

@Module({
  imports: [LoggerModule, ExceptionsModule, SmsSenderModule, DataServicesModule],
  providers: [JwtService],
  exports: [ExceptionsModule],
})
export class AccountUseCases {
  public static register(): DynamicModule {
    return {
      module: AccountUseCases,
      providers: [
        // Auth
        SignupUseCase,
        AuthUseCase,
        GetCurrentUserDataUseCase,
        ChangeUserRoleUseCase,
      ],
      exports: [
        // Auth
        SignupUseCase,
        AuthUseCase,
        GetCurrentUserDataUseCase,
        ChangeUserRoleUseCase,
      ],
    };
  }
}
