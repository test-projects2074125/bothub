import { Module } from "@nestjs/common";
// Controllers
import { AuthController } from "../controllers/account/auth/auth.controller";
// Module
import { JwtModule } from "@nestjs/jwt";
// Other
import { AccountUseCases } from "@bothub/infrastructure/usecase-modules/account-usecases.module";
import { JwtStrategy } from "@bothub/infrastructure/common";

@Module({
  imports: [
    JwtModule.register({
      signOptions: {
        algorithm: "HS256",
      },
    }),
    AccountUseCases.register(),
  ],
  controllers: [AuthController],
  providers: [JwtStrategy],
})
export class AccountModule {}
