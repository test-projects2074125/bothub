import { Injectable } from "@nestjs/common";
// Utils
import { isNil } from "lodash";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Other
import {
  BookPresenter,
  BookPresenterData,
} from "@bothub/infrastructure/controllers/library/books/presenters";
// Types and Enums
import { UseCase } from "@bothub/interfaces";

@Injectable()
export class GetBookByIdUseCase implements UseCase<BookPresenterData, number> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
  ) {}

  public async execute(bookId: number): Promise<BookPresenterData> {
    this.logger.log(GetBookByIdUseCase.name, `Getting book by id "${bookId}"`);
    //
    const book = await this.repository.book.findByPk(bookId);

    if (isNil(book)) {
      throw this.exceptionService.notFoundException("Книга не найдена");
    }
    return new BookPresenter(book);
  }
}
