import { Injectable } from "@nestjs/common";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
// Other
import {
  ListAllBooksPresenter,
  ListAllBooksPresenterData,
} from "@bothub/infrastructure/controllers/library/books/presenters";
// Types and Enums
import { UseCase } from "@bothub/interfaces";

@Injectable()
export class ListAllBooksUseCase implements UseCase<ListAllBooksPresenterData, void> {
  constructor(
    private readonly logger: LoggerService,
    private readonly repository: IDataServices,
  ) {}

  public async execute(): Promise<ListAllBooksPresenterData> {
    this.logger.log(ListAllBooksUseCase.name, "Listing all books");
    //
    const result = await this.repository.book.findAndCountAll();
    //
    return new ListAllBooksPresenter({
      rows: result.rows,
      total: result.count,
    });
  }
}
