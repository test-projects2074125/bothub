// eslint-disable-next-line @typescript-eslint/no-var-requires
const constants = require("../config/constants");

module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.query(`
      CREATE TABLE IF NOT EXISTS ${constants.tables.books} (
        id INT NOT NULL AUTO_INCREMENT,
        title VARCHAR(75) NOT NULL,
        author VARCHAR(50) NOT NULL,
        publicationDate DATE NOT NULL,
        genres VARCHAR(255) NOT NULL,

        PRIMARY KEY(id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    `),

  down: (queryInterface) =>
    queryInterface.sequelize.query(`DROP TABLE IF EXISTS ${constants.tables.books}`),
};
