import { Injectable } from "@nestjs/common";
// Utils
import { eq, isNil } from "lodash";
import {
  checkIfPublicationDateIsValid,
  parsePublicationDateToJSDate,
} from "@bothub/infrastructure/utils/common.util";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Other
import { UpdateBookDataDto } from "@bothub/infrastructure/controllers/library/books/dtos";
import {
  BookPresenter,
  BookPresenterData,
} from "@bothub/infrastructure/controllers/library/books/presenters";
// Types and Enums
import { UseCase } from "@bothub/interfaces";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";
import { BookCreation } from "@bothub/domain/interfaces/model";

type AddBookUseCaseArgs = {
  bookId: number;
  payload: JwtPayloadData;
  dto: UpdateBookDataDto;
};

@Injectable()
export class UpdateBookDataUseCase implements UseCase<BookPresenterData, AddBookUseCaseArgs> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
  ) {}

  public async execute({ bookId, payload, dto }: AddBookUseCaseArgs): Promise<BookPresenterData> {
    this.logger.log(
      UpdateBookDataUseCase.name,
      `Updating book data. Book id "${bookId}", Admin username "${payload.username}"`,
    );
    // Double check if the current user has admin role
    if (!payload.isAdmin()) {
      throw this.exceptionService.forbiddenException("Доступ запрещене");
    }
    // Check book existence
    const book = await this.repository.book.findByPk(bookId);

    if (isNil(book)) {
      throw this.exceptionService.notFoundException("Книга не найдена");
    }
    //
    const updateData: Partial<BookCreation> = {};

    // Check if publication date is valid
    if (!isNil(dto.title) && !eq(dto.title, book.title)) {
      updateData.title = dto.title;
    }
    if (!isNil(dto.author) && !eq(dto.author, book.author)) {
      updateData.author = dto.author;
    }
    if (!isNil(dto.publicationDate)) {
      if (!checkIfPublicationDateIsValid(dto.publicationDate)) {
        throw this.exceptionService.badRequestException("Некорректное значение даты");
      }
      updateData.publicationDate = parsePublicationDateToJSDate(dto.publicationDate);
    }
    if (!isNil(dto.genres) && !eq(dto.genres, book.genres)) {
      updateData.genres = dto.genres;
    }
    //
    await this.repository.book.update(updateData as any, {
      where: { id: bookId },
    });

    const updatedBook = await this.repository.book.findByPk(bookId);
    //
    return new BookPresenter(updatedBook!);
  }
}
