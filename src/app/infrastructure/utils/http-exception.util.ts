import { HttpException } from "@nestjs/common";
// Utils
import { eq } from "lodash";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
// Types
import { ExceptionData } from "@bothub/core/classes/exception-data.class";

export class HttpExceptionUtil {
  constructor(protected logger: LoggerService) {}

  public formatExceptionData(exception: HttpException | Error): ExceptionData {
    if (exception instanceof HttpException) {
      const exceptionResponse = exception.getResponse() as any;

      if (eq(exception.getStatus(), 429)) {
        return new ExceptionData(exception.message);
      }
      if (exceptionResponse instanceof ExceptionData) {
        return exceptionResponse;
      }
      if (typeof exceptionResponse === "object") {
        return ExceptionData.fromObject(exceptionResponse);
      }
    }
    this.logger.error(`An unexpected error occurred: ${exception.message}`);

    return new ExceptionData("An unexpected error occurred");
  }

  public logMessage(request: any, exceptionData: ExceptionData, status: number) {
    const context = `End Request for ${request.path}`;
    const message = `method=${request.method} status=${status} data=${exceptionData}`;

    if (status === 500) {
      this.logger.error(context, message);
    } else {
      this.logger.warn(context, message);
    }
  }
}
