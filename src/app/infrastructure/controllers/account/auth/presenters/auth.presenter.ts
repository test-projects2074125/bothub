import {
  UserPresenter,
  UserPresenterData,
  UserPresenterDataArgs,
} from "@bothub/infrastructure/controllers/account/auth/presenters/user.presenter";

export interface AuthPresenterData {
  user: UserPresenterData;
  accessToken: string;
}

export interface AuthPresenterDataArgs {
  user: UserPresenterDataArgs;
  accessToken: string;
}

export class AuthPresenter implements AuthPresenterData {
  public user: UserPresenterData;
  public accessToken: string;

  constructor(props: AuthPresenterDataArgs) {
    this.user = new UserPresenter(props.user);
    this.accessToken = props.accessToken;
  }
}
