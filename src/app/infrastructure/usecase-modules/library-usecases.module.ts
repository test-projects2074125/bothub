import { DynamicModule, Module } from "@nestjs/common";
// Modules and Services
import { LoggerModule } from "@bothub/infrastructure/services/logger/logger.module";
import { ExceptionsModule } from "@bothub/infrastructure/services/exception/exception.module";
import { DataServicesModule } from "@bothub/infrastructure/services/data-services/data-services.module";
// Use cases
import {
  AddBookUseCase,
  DeleteBookUseCase,
  GetBookByIdUseCase,
  ListAllBooksUseCase,
  UpdateBookDataUseCase,
} from "@bothub/infrastructure/usecases";

@Module({
  imports: [LoggerModule, ExceptionsModule, DataServicesModule],
  exports: [ExceptionsModule],
})
export class LibraryUseCases {
  public static register(): DynamicModule {
    return {
      module: LibraryUseCases,
      providers: [
        // Auth
        AddBookUseCase,
        ListAllBooksUseCase,
        GetBookByIdUseCase,
        UpdateBookDataUseCase,
        DeleteBookUseCase,
      ],
      exports: [
        // Auth
        AddBookUseCase,
        ListAllBooksUseCase,
        GetBookByIdUseCase,
        UpdateBookDataUseCase,
        DeleteBookUseCase,
      ],
    };
  }
}
