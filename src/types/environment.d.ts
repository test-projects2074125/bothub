import { AppProcessEnvVariables } from "@bothub/types/environment.interface";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";

declare global {
  namespace NodeJS {
    interface ProcessEnv extends AppProcessEnvVariables {}
  }
}

declare module "express" {
  export interface Request {
    user?: JwtPayloadData;
  }
}
export {};
