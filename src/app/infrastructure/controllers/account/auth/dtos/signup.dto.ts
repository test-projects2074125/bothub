import { IsEmail, IsNotEmpty, IsString, Length } from "class-validator";
import { ValidationMessages } from "@bothub/core/constants/validation-messages.constant";

export class SignupDto {
  @Length(6, 20, { message: ValidationMessages.minMaxStringLength(6, 20) })
  @IsString({ message: ValidationMessages.IS_STRING })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public username: string;

  @Length(6, 20, { message: ValidationMessages.minMaxStringLength(6, 20) })
  @IsString({ message: ValidationMessages.IS_STRING })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public password: string;

  @IsEmail({}, { message: ValidationMessages.IS_EMAIL })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public email: string;
}
