export interface Book {
  id: number;
  title: string;
  author: string;
  publicationDate: string;
  genres: string;
}

export interface BookCreation extends Omit<Book, "id" | "publicationDate"> {
  publicationDate: string | Date;
}
