// eslint-disable-next-line @typescript-eslint/no-var-requires
const constants = require("../config/constants");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    return queryInterface.bulkInsert(constants.tables.users, [
      {
        username: "Анатолий Комаров",
        email: "anatooliy_admin@gmail.com",
        password: "3fc0a7acf087f549ac2b266baf94b8b1", // qwerty123
        role: true,
        emailConfirmed: true,
      },
    ]);
  },

  async down(queryInterface) {
    return queryInterface.bulkDelete(
      constants.tables.users,
      { email: "anatooliy_admin@gmail.com" },
      {},
    );
  },
};
