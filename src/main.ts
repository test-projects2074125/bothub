import * as process from "node:process";
import { NestFactory, Reflector } from "@nestjs/core";
// Modules
import { AppModule } from "./app.module";
// Utils
import { isEmpty } from "lodash";
import { HttpStatus, Logger, UnprocessableEntityException, ValidationPipe } from "@nestjs/common";
import {
  AllExceptionFilter,
  LoggingInterceptor,
  RequestInterceptor,
  ResponseInterceptor,
} from "@bothub/infrastructure/common";
import { ValidationExceptionFilter } from "@bothub/infrastructure/common/filters/validation-exception.filter";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
// Environment variables
import { environment } from "@bothub/envs/environment";
import { appProcessEnvValidationSchema } from "@bothub/types/environment.interface";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  // Validating environment variables
  const result = appProcessEnvValidationSchema
    .options({ allowUnknown: true })
    .validate(process.env);

  if (result.error && !isEmpty(result.error.details)) {
    new LoggerService("App Startup").error(
      "Process env variables validation",
      result.error.details.at(0)?.message ?? "Process env variable validation error",
    );
    process.exit(9);
  }

  // Filters
  app.useGlobalFilters(new AllExceptionFilter(new LoggerService(AllExceptionFilter.name)));
  app.useGlobalFilters(
    new ValidationExceptionFilter(new LoggerService(ValidationExceptionFilter.name)),
  );

  // Pipes
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      exceptionFactory: (errors) => {
        return new UnprocessableEntityException({
          statusCode: HttpStatus.UNPROCESSABLE_ENTITY,
          error: "Validation Error",
          message: errors.reduce(
            (acc, e) => ({
              ...acc,
              [e.property]: Object.values(e.constraints as object),
            }),
            {},
          ),
        });
      },
    }),
  );

  // Interceptors
  app.useGlobalInterceptors(new LoggingInterceptor(new LoggerService(LoggingInterceptor.name)));
  app.useGlobalInterceptors(new RequestInterceptor(new Reflector()));
  app.useGlobalInterceptors(new ResponseInterceptor());

  await app.listen(environment.PORT);
  //
  Logger.log(`Application is running on: http://localhost:${environment.PORT}`);
}

bootstrap();
