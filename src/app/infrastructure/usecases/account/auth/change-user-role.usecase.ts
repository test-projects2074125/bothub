import { Injectable } from "@nestjs/common";
// Utils
import { assign, isNil } from "lodash";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Other
import {
  UserPresenter,
  UserPresenterData,
} from "@bothub/infrastructure/controllers/account/auth/presenters";
import { ChangeUserRoleDto } from "@bothub/infrastructure/controllers/account/auth/dtos";
// Types and Enums
import { UseCase } from "@bothub/interfaces";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";

type UseCaseArgs = {
  userId: number;
  payload: JwtPayloadData;
  dto: ChangeUserRoleDto;
};

@Injectable()
export class ChangeUserRoleUseCase implements UseCase<UserPresenterData, UseCaseArgs> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
  ) {}

  public async execute({ userId, payload, dto }: UseCaseArgs): Promise<UserPresenterData> {
    this.logger.log(ChangeUserRoleUseCase.name, `Changing user role with id "${userId}"`);
    // Double check if the user has Admin access
    if (!payload.isAdmin()) {
      throw this.exceptionService.forbiddenException("Доступ запрещен");
    }
    // Check if the admin is trying to change his role himself
    if (payload.id === userId) {
      throw this.exceptionService.badRequestException("Вы не можете изменить свою роль");
    }
    // Check user existence
    const user = await this.repository.user.findByPk(userId);

    if (isNil(user)) {
      throw this.exceptionService.notFoundException("Пользователь не найден");
    }
    // Return user data if new role is the same one as current
    if (user.role === dto.role) {
      return new UserPresenter(user);
    }
    //
    const [updatedCount] = await this.repository.user.update(
      {
        role: dto.role,
      },
      {
        where: { id: userId },
      },
    );
    //
    if (updatedCount === 0) {
      throw this.exceptionService.badRequestException(
        "При обновлении данных произошла неожиданная ошибка. Попробуйте позднее",
      );
    }
    return new UserPresenter(assign(user, { role: dto.role }));
  }
}
