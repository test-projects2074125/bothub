export interface IExceptionData<T = any> {
  message: string;
  description?: string | undefined;
  data?: T;
}

export interface ExceptionFilterResponse<T = any> {
  statusCode: number;
  timestamp: string;
  path: string;
  error: ExceptionResponseData<T>;
}

export interface ExceptionResponseData<T = any> extends IExceptionData<T> {
  isArray?: boolean | undefined;
}
