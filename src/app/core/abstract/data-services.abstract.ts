// Utils
import { assign, isBoolean } from "lodash";
// Models
import { BookModel, UserModel } from "@bothub/infrastructure/services/data-services/sql/models";
// Types
import { ModelType } from "@bothub/interfaces";
import { Sequelize } from "sequelize-typescript";
import { Transaction, TransactionOptions } from "sequelize";

export abstract class IDataServices {
  // Account
  public abstract user: ModelType<UserModel>;
  // Library
  public abstract book: ModelType<BookModel>;

  protected constructor(protected readonly sequelize: Sequelize) {}

  public startTransaction<T>(
    handler: (transaction: Transaction) => Promise<T>,
    options: TransactionOptions = {},
  ): Promise<T> {
    return new Promise<T>(async (resolve, reject) => {
      try {
        await this.sequelize.transaction(
          assign(
            {
              autocommit: isBoolean(options.autocommit) ? options.autocommit : true,
              isolationLevel: Transaction.ISOLATION_LEVELS.READ_COMMITTED,
            },
            options,
          ),
          async (t) => resolve(await handler(t)),
        );
      } catch (e) {
        reject(e);
      }
    });
  }
}
