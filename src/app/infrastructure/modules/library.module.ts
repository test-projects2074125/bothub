import { Module } from "@nestjs/common";
// Controllers
import { BooksController } from "../controllers/library/books/books.controller";
// Modules and Services
import { LibraryUseCases } from "@bothub/infrastructure/usecase-modules/library-usecases.module";
import { JwtStrategy } from "@bothub/infrastructure/common";

@Module({
  imports: [LibraryUseCases.register()],
  controllers: [BooksController],
  providers: [JwtStrategy],
})
export class LibraryModule {}
