import { IsBoolean, IsNotEmpty } from "class-validator";
import { Transform } from "class-transformer";
// Utils
import { isNumber } from "lodash";
import { ValidationMessages } from "@bothub/core/constants/validation-messages.constant";

export class ChangeUserRoleDto {
  @IsBoolean({ message: ValidationMessages.IS_BOOLEAN })
  @Transform(({ value }) => {
    if (isNumber(+value)) {
      return +value !== 0;
    }
    return value;
  })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public role: boolean;
}
