import { Inject, Injectable } from "@nestjs/common";
// Models
import { UserModel, BookModel } from "./sql/models";
// Utils
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ModelType } from "@bothub/interfaces";
import { Sequelize } from "sequelize-typescript";

@Injectable()
export class DataServices extends IDataServices {
  // Account
  public readonly user;
  // Library
  public readonly book;

  constructor(
    @Inject(Sequelize)
    sequelize: Sequelize,
    // Account
    @Inject(UserModel.name)
    UserRepositoryModel: ModelType<UserModel>,
    // Library
    @Inject(BookModel.name)
    BookRepositoryModel: ModelType<BookModel>,
  ) {
    super(sequelize);
    // Account
    this.user = UserRepositoryModel;
    // Library
    this.book = BookRepositoryModel;
  }
}
