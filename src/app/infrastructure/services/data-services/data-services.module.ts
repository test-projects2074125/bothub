import { Module } from "@nestjs/common";
// Data services
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { DataServices } from "./data-services.service";
// Modules
import { SqlDataServicesModule } from "./sql/sql-data-services.module";
import { LoggerModule } from "@bothub/infrastructure/services/logger/logger.module";

@Module({
  imports: [SqlDataServicesModule, LoggerModule],
  providers: [
    {
      provide: IDataServices,
      useClass: DataServices,
    },
  ],
  exports: [SqlDataServicesModule, IDataServices],
})
export class DataServicesModule {}
