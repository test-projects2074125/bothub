// Utils
import { isNull } from "lodash";

export interface UserPresenterData {
  id: number;
  username: string;
  email: string;
  isAdmin: boolean;
  emailConfirmed: boolean;
}

export interface UserPresenterDataArgs
  extends Omit<UserPresenterData, "isAdmin" | "emailConfirmed"> {
  role: boolean;
  emailConfirmationCode: string | null;
}

export class UserPresenter implements UserPresenterData {
  public id: number;
  public username: string;
  public email: string;
  public isAdmin: boolean;
  public emailConfirmed: boolean;

  constructor(props: UserPresenterDataArgs) {
    this.id = props.id;
    this.username = props.username;
    this.email = props.email;
    this.isAdmin = props.role;
    this.emailConfirmed = isNull(props.emailConfirmationCode);
  }
}
