import { Injectable } from "@nestjs/common";
// Utils
import { assign } from "lodash";
import {
  checkIfPublicationDateIsValid,
  parsePublicationDateToJSDate,
} from "@bothub/infrastructure/utils/common.util";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Other
import { AddBookDto } from "@bothub/infrastructure/controllers/library/books/dtos";
import {
  BookPresenter,
  BookPresenterData,
} from "@bothub/infrastructure/controllers/library/books/presenters";
// Types and Enums
import { UseCase } from "@bothub/interfaces";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";

type AddBookUseCaseArgs = {
  payload: JwtPayloadData;
  dto: AddBookDto;
};

@Injectable()
export class AddBookUseCase implements UseCase<BookPresenterData, AddBookUseCaseArgs> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
  ) {}

  public async execute({ payload, dto }: AddBookUseCaseArgs): Promise<BookPresenterData> {
    this.logger.log(AddBookUseCase.name, `Adding book. Admin username "${payload.username}"`);

    // Double check if the current user has admin role
    if (!payload.isAdmin()) {
      throw this.exceptionService.forbiddenException("Доступ запрещене");
    }
    // Check if publication date is valid
    if (!checkIfPublicationDateIsValid(dto.publicationDate)) {
      throw this.exceptionService.badRequestException("Некорректное значение даты");
    }
    //
    const newBook = await this.repository.book.create(
      assign(dto, {
        publicationDate: parsePublicationDateToJSDate(dto.publicationDate),
      }),
    );
    //
    return new BookPresenter(newBook);
  }
}
