export class ValidationMessages {
  protected constructor() {}

  // Constants
  public static readonly IS_NOT_EMPTY = "Это поле обязательно к заполнению";
  public static readonly IS_BOOLEAN = "Это поле должно содержать булевое значение";
  public static readonly IS_STRING = "Это поле должно содержать строку";
  public static readonly IS_NUMBER = "Это поле должно содержать число";
  public static readonly IS_EMAIL = "Некорректный электронный адрес";
  public static readonly IS_DATE = "Это поле должно содержать дату в формате День.Месяц.Год";

  // Methods
  public static minMaxStringLength(min: number, max: number): string {
    return `Строка должна состоять от ${min} до ${max} символов`;
  }
}
