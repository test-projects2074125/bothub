import { Reflector } from "@nestjs/core";
import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
// Utils
import { isEmpty, isNil, isUndefined } from "lodash";
import { AcceptedRoles } from "@bothub/infrastructure/common/decorators/roles-guard.decorator";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";
// Services
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Types and Enums
import { Request } from "express";

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private exceptionService: ExceptionService,
  ) {}

  public canActivate(context: ExecutionContext): boolean {
    const roles: boolean[] = this.reflector.get(AcceptedRoles, context.getHandler());

    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest() as Request;

    if (isNil(request.user)) {
      return false;
    }
    // Return true if user can activate(has permissions)
    if (this.checkPermission(roles, request.user)) {
      return true;
    }
    throw this.exceptionService.forbiddenException("Доступ запрещен");
  }

  private checkPermission(roles: boolean[], user: JwtPayloadData): boolean {
    if (isEmpty(roles) || isUndefined(user.role)) {
      return false;
    }
    return roles.includes(user.role);
  }
}
