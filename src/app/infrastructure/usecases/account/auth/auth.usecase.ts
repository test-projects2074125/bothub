import * as crypto from "node:crypto";
import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
// Utils
import { eq, isNil } from "lodash";
import { environment } from "@bothub/envs/environment";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Other
import {
  AuthPresenter,
  AuthPresenterData,
} from "@bothub/infrastructure/controllers/account/auth/presenters";
import { AuthDto } from "@bothub/infrastructure/controllers/account/auth/dtos";
// Types and Enums
import { JwtPayload, UseCase } from "@bothub/interfaces";

@Injectable()
export class AuthUseCase implements UseCase<AuthPresenterData, AuthDto> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
    private readonly jwtService: JwtService,
  ) {}

  public async execute({ username, password }: AuthDto): Promise<AuthPresenterData> {
    this.logger.log(AuthUseCase.name, `Authenticating user with username "${username}"`);
    //
    const user = await this.repository.user.findOne({ where: { username } });

    // Check user existence
    if (isNil(user)) {
      throw this.exceptionService.notFoundException("Пользователь не найден");
    }
    // Check user password
    const passwordHash = crypto.createHash("md5").update(password).digest("hex");

    if (!eq(user.password, passwordHash)) {
      throw this.exceptionService.unauthorizedException("Неправильный логин, или пароль");
    }
    // Create JWT access token
    const accessToken = await this.jwtService.signAsync(
      {
        id: user.id,
        username: user.username,
        email: user.email,
        role: user.role,
      } satisfies JwtPayload,
      {
        secret: environment.JWT_ACCESS_TOKEN_SECRET,
      },
    );
    //
    return new AuthPresenter({ user, accessToken });
  }
}
