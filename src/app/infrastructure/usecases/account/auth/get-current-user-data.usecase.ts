import { Injectable } from "@nestjs/common";
// Utils
import { isNil } from "lodash";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Other
import {
  UserPresenter,
  UserPresenterData,
} from "@bothub/infrastructure/controllers/account/auth/presenters";
// Types and Enums
import { UseCase } from "@bothub/interfaces";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";

@Injectable()
export class GetCurrentUserDataUseCase implements UseCase<UserPresenterData, JwtPayloadData> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
  ) {}

  public async execute(payload: JwtPayloadData): Promise<UserPresenterData> {
    this.logger.log(
      GetCurrentUserDataUseCase.name,
      `Getting current user data with username "${payload.username}"`,
    );
    //
    const user = await this.repository.user.findByPk(payload.id);

    // Check user existence
    if (isNil(user)) {
      throw this.exceptionService.badRequestException(
        "Произошла неожиданная ошибка. Данные о пользователе не найдены.",
      );
    }
    //
    return new UserPresenter(user);
  }
}
