import {
  AutoIncrement,
  Column,
  DataType,
  Model,
  NotNull,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
// Types
import { Book, BookCreation } from "@bothub/domain/interfaces/model";

@Table({
  tableName: "books",
  charset: "utf8",
  timestamps: false,
})
export class BookModel extends Model<Book, BookCreation> implements Book {
  @PrimaryKey
  @AutoIncrement
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  public override id: number;

  @NotNull
  @Column({
    type: DataType.STRING(75),
    allowNull: false,
  })
  public title: string;

  @NotNull
  @Column({
    type: DataType.STRING(50),
    allowNull: false,
  })
  public author: string;

  @NotNull
  @Column({
    type: DataType.DATEONLY,
    allowNull: false,
  })
  public publicationDate: string;

  @NotNull
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public genres: string;
}
