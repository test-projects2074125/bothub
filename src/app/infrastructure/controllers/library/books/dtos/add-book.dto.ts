import { IsNotEmpty, IsString, Length, Matches } from "class-validator";
import { ValidationMessages } from "@bothub/core/constants/validation-messages.constant";
import { GlobalConstants } from "@bothub/core/constants/global.constants";

export class AddBookDto {
  @Length(6, 75, { message: ValidationMessages.minMaxStringLength(6, 75) })
  @IsString({ message: ValidationMessages.IS_STRING })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public title: string;

  @Length(6, 50, { message: ValidationMessages.minMaxStringLength(6, 50) })
  @IsString({ message: ValidationMessages.IS_STRING })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public author: string;

  @Matches(GlobalConstants.PUBLICATION_DATE_REGEXP, { message: ValidationMessages.IS_DATE })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public publicationDate: string;

  @IsString({ message: ValidationMessages.IS_STRING })
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public genres: string;
}
