import { Injectable } from "@nestjs/common";
// Utils
import { createTransport, Transporter } from "nodemailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";
import Mail from "nodemailer/lib/mailer";
import { environment } from "@bothub/envs/environment";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";

type Email = {
  transport: Transporter<SMTPTransport.SentMessageInfo>;
  sender: Mail.Address;
};

@Injectable()
export class MailService {
  private readonly noReply: Email;

  constructor(private readonly logger: LoggerService) {
    this.noReply = {
      transport: createTransport({
        host: environment.MAIL_HOST,
        port: 465,
        secure: true,
        auth: {
          user: environment.NO_REPLY_EMAIL,
          pass: environment.NO_REPLY_EMAIL_PASSWORD,
        },
      }),
      sender: {
        name: "BotHub",
        address: environment.NO_REPLY_EMAIL,
      },
    };
  }

  /** @returns true if email was successfully sent, else false **/
  public async sendActivationCodeMail(email: string, code: string): Promise<boolean> {
    try {
      await this.noReply.transport.sendMail({
        from: this.noReply.sender,
        to: email,
        subject: "Подтверждение электронной почты",
        html: `
        <div>
          <p>Ваш код для подтверждения электронной почты в приложении BotHub: ${code}</p>
        </div>
        `,
      });

      return true;
    } catch (e) {
      this.logger.warn(
        MailService.name,
        `[sendActivationCodeMail](${email}, ${code}) An error occurred: ${e}`,
      );
      return false;
    }
  }
}
