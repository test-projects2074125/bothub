import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
// Types
import { JwtPayload } from "@bothub/interfaces";

@Injectable()
export class JwtAuthGuard extends AuthGuard("jwt-auth") {
  public override handleRequest<User extends JwtPayload>(err: Error | null, user: User) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
