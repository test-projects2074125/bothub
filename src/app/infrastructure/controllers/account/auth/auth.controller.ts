import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
} from "@nestjs/common";
// Utils
import { JwtAuthGuard, SetRequestTimeout } from "@bothub/infrastructure/common";
import {
  UseJwtAuthWithRoleGuards,
  UserPermissions,
} from "@bothub/infrastructure/common/decorators/roles-guard.decorator";
// Use cases
import {
  AuthUseCase,
  ChangeUserRoleUseCase,
  GetCurrentUserDataUseCase,
  SignupUseCase,
} from "@bothub/infrastructure/usecases";
// DTO
import {
  AuthDto,
  ChangeUserRoleDto,
  SignupDto,
} from "@bothub/infrastructure/controllers/account/auth/dtos";
import { IntIdDto } from "@bothub/infrastructure/controllers/dtos/int-id.dto";
// Types
import { Request } from "express";
import {
  AuthPresenterData,
  UserPresenterData,
} from "@bothub/infrastructure/controllers/account/auth/presenters";

@Controller("/users")
export class AuthController {
  constructor(
    private readonly signupUseCase: SignupUseCase,
    private readonly authUserUseCase: AuthUseCase,
    private readonly getCurrentUserDataUseCase: GetCurrentUserDataUseCase,
    private readonly changeUserRoleUseCase: ChangeUserRoleUseCase,
  ) {}

  @Post("/register")
  @SetRequestTimeout(15000)
  public signupUser(@Body() signupDto: SignupDto): Promise<UserPresenterData> {
    return this.signupUseCase.execute(signupDto);
  }

  @Post("/login")
  public authenticateUser(@Body() authDto: AuthDto): Promise<AuthPresenterData> {
    return this.authUserUseCase.execute(authDto);
  }

  @Get("/me")
  @UseGuards(JwtAuthGuard)
  public getCurrentUserData(@Req() req: Request): Promise<UserPresenterData> {
    return this.getCurrentUserDataUseCase.execute(req.user!);
  }

  @Put("/:id/role")
  @UseJwtAuthWithRoleGuards(UserPermissions.account.user.changeRole)
  @HttpCode(HttpStatus.OK)
  public changeUserRole(
    @Req() req: Request,
    @Param() { id }: IntIdDto,
    @Body() changeUserRoleDto: ChangeUserRoleDto,
  ): Promise<UserPresenterData> {
    return this.changeUserRoleUseCase.execute({
      userId: id,
      payload: req.user!,
      dto: changeUserRoleDto,
    });
  }
}
