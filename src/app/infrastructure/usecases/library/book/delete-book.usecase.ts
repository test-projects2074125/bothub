import { Injectable } from "@nestjs/common";
// Utils
import { isNil } from "lodash";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Types and Enums
import { UseCase } from "@bothub/interfaces";
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";

type AddBookUseCaseArgs = {
  payload: JwtPayloadData;
  bookId: number;
};

@Injectable()
export class DeleteBookUseCase implements UseCase<void, AddBookUseCaseArgs> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
  ) {}

  public async execute({ payload, bookId }: AddBookUseCaseArgs): Promise<void> {
    this.logger.log(DeleteBookUseCase.name, `Deleting book. Admin username "${payload.username}"`);

    // Double check if the current user has admin role
    if (!payload.isAdmin()) {
      throw this.exceptionService.forbiddenException("Доступ запрещене");
    }
    // Check book existence
    const book = await this.repository.book.findByPk(bookId);

    if (isNil(book)) {
      throw this.exceptionService.notFoundException("Книга не найдена");
    }
    //
    const deletedCount = await this.repository.book.destroy({
      where: { id: bookId },
    });

    // Check if deleted successfully
    if (deletedCount === 0) {
      throw this.exceptionService.badRequestException(
        "При удалении книги произошла неоижиданная ошибка. Попробуйте позднее",
      );
    }
  }
}
