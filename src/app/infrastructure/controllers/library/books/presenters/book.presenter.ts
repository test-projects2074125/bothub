export interface BookPresenterData {
  id: number;
  title: string;
  author: string;
  publicationDate: string;
  genres: string;
}

export class BookPresenter implements BookPresenterData {
  public id: number;
  public title: string;
  public author: string;
  public publicationDate: string;
  public genres: string;

  constructor(props: BookPresenterData) {
    this.id = props.id;
    this.title = props.title;
    this.author = props.author;
    this.publicationDate = props.publicationDate;
    this.genres = props.genres;
  }
}
