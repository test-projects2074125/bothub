import { IsNotEmpty, IsNumber } from "class-validator";
import { ValidationMessages } from "@bothub/core/constants/validation-messages.constant";
import { Transform } from "class-transformer";

export class IntIdDto {
  @IsNumber({ allowNaN: false, allowInfinity: false }, { message: ValidationMessages.IS_NUMBER })
  @Transform(({ value }) => +value)
  @IsNotEmpty({ message: ValidationMessages.IS_NOT_EMPTY })
  public id: number;
}
