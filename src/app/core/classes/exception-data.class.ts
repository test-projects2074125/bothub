import * as _ from "lodash";
// Types
import { ExceptionResponseData, IExceptionData } from "@bothub/interfaces";

type DynamicExceptionDataObject = { [Property in keyof IExceptionData]: any } & {
  [key: string | symbol]: any;
};

export class ExceptionData implements IExceptionData {
  private static defaultMessage = "Error occurred";
  public readonly message: string;
  public readonly description: string | undefined;
  public readonly data?: any;

  constructor(
    error?: Partial<IExceptionData> | string,
    defaultMessage = ExceptionData.defaultMessage,
  ) {
    if (error) {
      if (typeof error === "string") {
        this.message = error;
      } else {
        this.message = error.message ?? defaultMessage;
        this.description = error.description;
        this.data = error.data;
      }
    } else {
      this.message = defaultMessage;
    }
  }

  public static fromObject(data: DynamicExceptionDataObject): ExceptionData {
    if (data["message"] instanceof Array) {
      const error: Partial<IExceptionData> = {
        message: ExceptionData.defaultMessage,
        description: data["description"],
        data: data["message"],
      };

      if (typeof data["error"] === "string") {
        error.message = data["error"];
      }
      return new ExceptionData(error);
    } else if (!data["message"] && typeof data["error"] === "string") {
      return new ExceptionData({
        message: data["error"],
        description: data["description"],
        data: data["data"],
      });
    }
    return new ExceptionData({
      message: data["message"] ?? ExceptionData.defaultMessage,
      description: data["description"],
      data: data["data"],
    });
  }

  public buildResponse(): ExceptionResponseData {
    return {
      message: this.message,
      description: this.description,
      data: this.data,
      isArray: this.data ? this.data instanceof Array : undefined,
    };
  }

  private dataToString(): string | null {
    if (!this.data) {
      return null;
    }
    if (_.isObject(this.data)) {
      let dataString = "{";

      for (const key in this.data) {
        dataString += ` ${key}: ${(this.data as Record<any, any>)[key]},`;
      }
      dataString = dataString.substring(0, dataString.lastIndexOf(",")).concat(" }");

      return dataString;
    }
    return _.toString(this.data);
  }

  public toString(): string {
    let string = `${ExceptionData.name}(message: ${this.message}`;

    if (_.isString(this.description)) {
      string += `, description: ${this.description}`;
    }
    if (!_.isUndefined(this.data) && !_.isNull(this.data)) {
      string += `, data: ${this.dataToString()}`;
    }
    string += ")";

    return string;
  }
}
