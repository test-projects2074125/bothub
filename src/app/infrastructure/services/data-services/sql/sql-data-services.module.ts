import { Module } from "@nestjs/common";
import { SequelizeModule, getConnectionToken } from "@nestjs/sequelize";
import { Sequelize } from "sequelize-typescript";
// Models
import { UserModel, BookModel } from "./models";
// Utils
import { environment } from "@bothub/envs/environment";
import { Environment } from "@bothub/types/environment.interface";

@Module({
  imports: [
    SequelizeModule.forRoot({
      name: "BotHub",
      define: {
        charset: "utf8",
        collate: "utf8_general_ci",
        timestamps: true,
      },
      dialect: environment.SQL_DB.DIALECT,
      host: environment.SQL_DB.HOST,
      port: environment.SQL_DB.PORT,
      username: environment.SQL_DB.USERNAME,
      password: environment.SQL_DB.PASSWORD,
      database: environment.SQL_DB.DATABASE,
      timezone: "+00:00",
      logging: environment.NODE_ENV === Environment.Development ? console.log : false,
      retry: {
        max: 5,
      },
      models: [
        // Account
        UserModel,
        // Library
        BookModel,
      ],
      autoLoadModels: false,
      sync: {
        logging: true,
        force: false,
        alter: false,
      },
    }),
  ],
  providers: [
    {
      provide: Sequelize,
      inject: [getConnectionToken("BotHub")],
      useFactory: (sequelize: Sequelize) => sequelize,
    },
    // Account
    {
      provide: UserModel.name,
      useValue: UserModel,
    },
    // Library
    {
      provide: BookModel.name,
      useValue: BookModel,
    },
  ],
  exports: [
    Sequelize,
    // Account
    UserModel.name,
    // Library
    BookModel.name,
  ],
})
export class SqlDataServicesModule {}
