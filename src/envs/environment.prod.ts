import * as process from "node:process";
// Types
import { AppEnvironment } from "@bothub/types/environment.interface";

export const prodEnv: AppEnvironment = {
  PORT: 3000,
  SQL_DB: {
    DIALECT: "mysql",
    HOST: "localhost",
    PORT: 3306,
    USERNAME: "root",
    PASSWORD: "JWWbqDk~/4*%]LM",
    DATABASE: "bothub_db",
  },
  NODE_ENV: process.env.NODE_ENV,
  HOST: process.env.HOST,
  JWT_ACCESS_TOKEN_SECRET: process.env.JWT_ACCESS_TOKEN_SECRET,
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
  MAIL_HOST: process.env.MAIL_HOST,
  NO_REPLY_EMAIL: process.env.NO_REPLY_EMAIL,
  NO_REPLY_EMAIL_PASSWORD: process.env.NO_REPLY_EMAIL_PASSWORD,
};
