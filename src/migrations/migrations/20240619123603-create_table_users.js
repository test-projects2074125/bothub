// eslint-disable-next-line @typescript-eslint/no-var-requires
const constants = require("../config/constants");

module.exports = {
  up: (queryInterface) =>
    queryInterface.sequelize.query(`
      CREATE TABLE IF NOT EXISTS ${constants.tables.users} (
        id INT NOT NULL AUTO_INCREMENT,
        username VARCHAR(20) NOT NULL,
        email VARCHAR(75) NOT NULL,
        password VARCHAR(32) NOT NULL,
        role BOOLEAN NOT NULL DEFAULT FALSE,
        emailConfirmationCode VARCHAR(255) NOT NULL,

        PRIMARY KEY(id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    `),

  down: (queryInterface) =>
    queryInterface.sequelize.query(`DROP TABLE IF EXISTS ${constants.tables.users}`),
};
