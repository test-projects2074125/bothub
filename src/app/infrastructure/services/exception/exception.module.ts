import { Module } from "@nestjs/common";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
import { ExceptionServiceImpl } from "@bothub/infrastructure/services/exception/exception.service";

@Module({
  providers: [
    {
      provide: ExceptionService,
      useClass: ExceptionServiceImpl,
    },
  ],
  exports: [ExceptionService],
})
export class ExceptionsModule {}
