import { IsOptional, IsString, Length, Matches } from "class-validator";
import { ValidationMessages } from "@bothub/core/constants/validation-messages.constant";
import { GlobalConstants } from "@bothub/core/constants/global.constants";

export class UpdateBookDataDto {
  @Length(6, 75, { message: ValidationMessages.minMaxStringLength(6, 75) })
  @IsString({ message: ValidationMessages.IS_STRING })
  @IsOptional()
  public title?: string;

  @Length(6, 50, { message: ValidationMessages.minMaxStringLength(6, 50) })
  @IsString({ message: ValidationMessages.IS_STRING })
  @IsOptional()
  public author?: string;

  @Matches(GlobalConstants.PUBLICATION_DATE_REGEXP, { message: ValidationMessages.IS_DATE })
  @IsOptional()
  public publicationDate?: string;

  @IsString({ message: ValidationMessages.IS_STRING })
  @IsOptional()
  public genres?: string;
}
