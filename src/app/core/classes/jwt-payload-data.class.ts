import { JwtPayload } from "@bothub/interfaces";
import { UserRole } from "@bothub/core/constants/common.enums";

export class JwtPayloadData implements JwtPayload {
  public readonly id: JwtPayload["id"];
  public readonly username: JwtPayload["username"];
  public readonly email: JwtPayload["email"];
  public readonly role: JwtPayload["role"];

  constructor(props: JwtPayload) {
    this.id = props.id;
    this.username = props.username;
    this.email = props.email;
    this.role = props.role;
  }

  public isAdmin(): boolean {
    return this.role === UserRole.Admin;
  }
}
