export interface User {
  id: number;
  username: string;
  email: string;
  password: string;
  role: boolean;
  emailConfirmationCode: string | null;
}

export interface UserCreation extends Omit<User, "id" | "role" | "emailConfirmationCode"> {}
