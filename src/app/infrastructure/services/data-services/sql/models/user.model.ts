import {
  AllowNull,
  AutoIncrement,
  Column,
  DataType,
  Model,
  NotNull,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import * as crypto from "node:crypto";
// Types
import { User, UserCreation } from "@bothub/domain/interfaces/model";

@Table({
  tableName: "users",
  charset: "utf8",
  timestamps: false,
})
export class UserModel extends Model<User, UserCreation> implements User {
  @PrimaryKey
  @AutoIncrement
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  public override id: number;

  @NotNull
  @Column({
    type: DataType.STRING(20),
    allowNull: false,
  })
  public username: string;

  @NotNull
  @Column({
    type: DataType.STRING(75),
    allowNull: false,
    unique: true,
  })
  public email: string;

  @NotNull
  @Column({
    type: DataType.STRING(32),
    allowNull: false,
  })
  public password: string;

  @NotNull
  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  public role: boolean;

  @AllowNull
  @Column({
    type: DataType.STRING(255),
    allowNull: true,
    defaultValue: () => crypto.randomBytes(20).toString("hex"),
  })
  public emailConfirmationCode: string | null;
}
