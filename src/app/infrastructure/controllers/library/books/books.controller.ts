import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Req,
} from "@nestjs/common";
// Utils
import {
  UseJwtAuthWithRoleGuards,
  UserPermissions,
} from "@bothub/infrastructure/common/decorators/roles-guard.decorator";
// Use cases
import {
  AddBookUseCase,
  DeleteBookUseCase,
  GetBookByIdUseCase,
  ListAllBooksUseCase,
  UpdateBookDataUseCase,
} from "@bothub/infrastructure/usecases";
// DTO
import {
  AddBookDto,
  UpdateBookDataDto,
} from "src/app/infrastructure/controllers/library/books/dtos";
import { IntIdDto } from "@bothub/infrastructure/controllers/dtos/int-id.dto";
// Types
import { Request } from "express";
import {
  BookPresenterData,
  ListAllBooksPresenterData,
} from "@bothub/infrastructure/controllers/library/books/presenters";

@Controller("/books")
export class BooksController {
  constructor(
    private readonly addBookUseCase: AddBookUseCase,
    private readonly listAllBooksUseCase: ListAllBooksUseCase,
    private readonly getBookByIdUseCase: GetBookByIdUseCase,
    private readonly updateBookDataUseCase: UpdateBookDataUseCase,
    private readonly deleteBookUseCase: DeleteBookUseCase,
  ) {}

  @Post()
  @UseJwtAuthWithRoleGuards(UserPermissions.library.books.add)
  public addBook(@Req() req: Request, @Body() addBookDto: AddBookDto): Promise<BookPresenterData> {
    return this.addBookUseCase.execute({
      payload: req.user!,
      dto: addBookDto,
    });
  }

  @Get()
  public listBooks(): Promise<ListAllBooksPresenterData> {
    return this.listAllBooksUseCase.execute();
  }

  @Get("/:id")
  public getBookById(@Param() { id }: IntIdDto): Promise<BookPresenterData> {
    return this.getBookByIdUseCase.execute(id);
  }

  @Put("/:id")
  @UseJwtAuthWithRoleGuards(UserPermissions.library.books.update)
  @HttpCode(HttpStatus.OK)
  public updateBookData(
    @Req() req: Request,
    @Param() { id }: IntIdDto,
    @Body() updateBookDto: UpdateBookDataDto,
  ): Promise<BookPresenterData> {
    return this.updateBookDataUseCase.execute({
      bookId: id,
      payload: req.user!,
      dto: updateBookDto,
    });
  }

  @Delete("/:id")
  @UseJwtAuthWithRoleGuards(UserPermissions.library.books.delete)
  @HttpCode(HttpStatus.NO_CONTENT)
  public deleteBook(@Req() req: Request, @Param() { id }: IntIdDto): Promise<void> {
    return this.deleteBookUseCase.execute({
      bookId: id,
      payload: req.user!,
    });
  }
}
