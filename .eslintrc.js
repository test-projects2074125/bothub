module.exports = {
  root: true,
  parser: "@typescript-eslint/parser",
  plugins: ["prettier", "@typescript-eslint/eslint-plugin"],
  extends: ["prettier", "plugin:@typescript-eslint/recommended"],
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: [".eslintrc.js", "dist", "test", "webpack.config.js", "**/generated.ts"],
  overrides: [
    {
      files: ["*.ts", "*.tsx"],
      extends: ["prettier", "plugin:@typescript-eslint/recommended"],
      parserOptions: {
        project: ['./tsconfig.json'],
      },
      rules: {
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-var-requires": "off",
        "@typescript-eslint/no-empty-interface": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
        "@typescript-eslint/no-namespace": "off",
        "@typescript-eslint/no-extra-non-null-assertion": "error",
        "@typescript-eslint/no-confusing-non-null-assertion": "error",
        "@typescript-eslint/non-nullable-type-assertion-style": "error",
        "@typescript-eslint/consistent-type-assertions": ["error", { assertionStyle: 'as', objectLiteralTypeAssertions: 'allow-as-parameter' }],
        "@typescript-eslint/explicit-member-accessibility": ["error", { overrides: { constructors: "no-public" } }],
        "@typescript-eslint/no-unused-vars": ["error", { "vars": "all", "args": "none", "ignoreRestSiblings": true }],
        "no-async-promise-executor": 0
      },
    },
    {
      files: ["*.js", "*.jsx"],
      extends: ["prettier"],
    },
  ],
  rules: {
    "prettier/prettier": "error",
    "lines-between-class-members": ["error", "always", { "exceptAfterSingleLine": true }],
    semi: "error",
    "no-var": "off",
    curly: "error",
    eqeqeq: "warn",
    quotes: ["error", "double"],
    "max-len": [0, 100, 2, { ignoreUrls: true }],
    "no-unused-vars": ["error", { "vars": "all", "args": "none", "ignoreRestSiblings": false }]
  },
};
