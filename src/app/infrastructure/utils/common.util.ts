export function checkIfPublicationDateIsValid(publicationDate: string): boolean {
  const publicationDateValues = publicationDate.split(".");
  const day: number = +publicationDateValues[0]!;
  const month: number = +publicationDateValues[1]!;
  const year: number = +publicationDateValues[2]!;

  if (
    day > 31 ||
    day < 1 || // Day
    month > 12 ||
    month < 1 || // Month
    year > new Date().getFullYear() ||
    year < 1800 // Year
  ) {
    return false;
  }
  return true;
}

export function parsePublicationDateToJSDate(publicationDate: string): Date {
  const publicationDateValues = publicationDate.split(".");
  const day: number = +publicationDateValues[0]!;
  const month: number = +publicationDateValues[1]!;
  const year: number = +publicationDateValues[2]!;

  return new Date(year, month - 1, day);
}
