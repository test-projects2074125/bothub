import * as Joi from "joi";
import { Dialect } from "sequelize";

export enum Environment {
  Development = "development",
  Production = "production",
  Test = "test",
}

// Process environment variables
export type SqlDB = {
  DIALECT: Dialect;
  HOST: string;
  PORT: number;
  USERNAME: string;
  PASSWORD: string;
  DATABASE: string;
};

export interface AppEnvironment extends AppProcessEnvVariables {
  PORT: number;
  SQL_DB: SqlDB;
}

export interface AppProcessEnvVariables {
  NODE_ENV: Environment;
  HOST: string;
  JWT_ACCESS_TOKEN_SECRET: string;
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: string;
  MAIL_HOST: string;
  NO_REPLY_EMAIL: string;
  NO_REPLY_EMAIL_PASSWORD: string;
}

export const appProcessEnvValidationSchema: Joi.Schema = Joi.object({
  NODE_ENV: Joi.string().valid(Environment.Test, Environment.Development, Environment.Production),
  HOST: Joi.string().required(),
  JWT_ACCESS_TOKEN_SECRET: Joi.string().required(),
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: Joi.string().required(),
  MAIL_HOST: Joi.string().required(),
  NO_REPLY_EMAIL: Joi.string().required(),
  NO_REPLY_EMAIL_PASSWORD: Joi.string().required(),
} as Required<{ [Key in keyof AppProcessEnvVariables]: Joi.AnySchema }>);
