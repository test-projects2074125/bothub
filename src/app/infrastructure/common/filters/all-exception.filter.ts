import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from "@nestjs/common";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
// Utils
import { HttpExceptionUtil } from "@bothub/infrastructure/utils";
// Types
import { Response } from "express";
import { ExceptionData } from "@bothub/core/classes/exception-data.class";
import { ExceptionFilterResponse } from "@bothub/interfaces";

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  public readonly httpExceptionUtil: HttpExceptionUtil;

  constructor(logger: LoggerService) {
    this.httpExceptionUtil = new HttpExceptionUtil(logger);
  }

  public catch(exception: HttpException | Error, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse() as Response<ExceptionFilterResponse>;
    const request: any = ctx.getRequest();

    const status =
      exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

    const exceptionData: ExceptionData = this.httpExceptionUtil.formatExceptionData(exception);

    this.httpExceptionUtil.logMessage(request, exceptionData, status);

    response.status(status).json({
      statusCode: status,
      timestamp: new Date().toISOString(),
      path: request.url,
      error: exceptionData.buildResponse(),
    });
  }
}
