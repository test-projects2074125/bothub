import { HttpException } from "@nestjs/common";
// Types
import { IExceptionData } from "@bothub/interfaces";

export abstract class ExceptionService {
  public abstract notFoundException(data: IExceptionData | string): HttpException;
  public abstract badRequestException(data: IExceptionData | string): HttpException;
  public abstract forbiddenException(data?: IExceptionData | string): HttpException;
  public abstract unauthorizedException(data?: IExceptionData | string): HttpException;
}
