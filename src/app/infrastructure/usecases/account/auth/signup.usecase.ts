import * as crypto from "node:crypto";
import { Injectable } from "@nestjs/common";
// Utils
import { isNil } from "lodash";
// Services
import { LoggerService } from "@bothub/infrastructure/services/logger/logger.service";
import { IDataServices } from "@bothub/core/abstract/data-services.abstract";
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
import { MailService } from "@bothub/infrastructure/services/sms-sender/mail.service";
// Other
import {
  UserPresenter,
  UserPresenterData,
} from "@bothub/infrastructure/controllers/account/auth/presenters";
import { SignupDto } from "@bothub/infrastructure/controllers/account/auth/dtos";
// Types and Enums
import { UseCase } from "@bothub/interfaces";

@Injectable()
export class SignupUseCase implements UseCase<UserPresenterData, SignupDto> {
  constructor(
    private readonly logger: LoggerService,
    private readonly exceptionService: ExceptionService,
    private readonly repository: IDataServices,
    private readonly mailService: MailService,
  ) {}

  public async execute({ username, email, password }: SignupDto): Promise<UserPresenterData> {
    this.logger.log(SignupUseCase.name, `Registering user with username "${username}"`);

    // Check if the email is busy
    const userWithProvidedEmail = await this.repository.user.findOne({ where: { email } });

    if (!isNil(userWithProvidedEmail)) {
      throw this.exceptionService.badRequestException(
        "Пользователь c указанной эл. почтой уже зарегистрирован",
      );
    }
    //
    return this.repository.startTransaction<UserPresenter>(async (transaction) => {
      try {
        const newUser = await this.repository.user.create(
          {
            username,
            email,
            password: crypto.createHash("md5").update(password).digest("hex"),
          },
          { transaction },
        );
        // Send email confirmation mail
        const sent = await this.mailService.sendActivationCodeMail(
          newUser.email,
          newUser.emailConfirmationCode!,
        );

        if (!sent) {
          throw this.exceptionService.badRequestException(
            "При отправке эл. письма произошла неожиданная ошибка. Попробуйте позднее",
          );
        }
        await transaction.commit();
        //
        return new UserPresenter(newUser);
      } catch (exc) {
        await transaction.rollback();
        //
        throw exc;
      }
    });
  }
}
