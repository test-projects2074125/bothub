import {
  BadRequestException,
  ForbiddenException,
  HttpException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from "@nestjs/common";
// Services
import { ExceptionService } from "@bothub/core/abstract/exception-service.abstract";
// Utils
import { ExceptionData } from "@bothub/core/classes/exception-data.class";
// Types
import { IExceptionData } from "@bothub/interfaces";

@Injectable()
export class ExceptionServiceImpl extends ExceptionService {
  public override notFoundException(data: IExceptionData | string): HttpException {
    return new NotFoundException(new ExceptionData(data));
  }

  public override badRequestException(data: IExceptionData | string): HttpException {
    return new BadRequestException(new ExceptionData(data));
  }

  public override forbiddenException(data?: IExceptionData | string): HttpException {
    return new ForbiddenException(new ExceptionData(data, "Forbidden"));
  }

  public override unauthorizedException(data?: IExceptionData | string): HttpException {
    return new UnauthorizedException(new ExceptionData(data, "Unauthorized"));
  }
}
