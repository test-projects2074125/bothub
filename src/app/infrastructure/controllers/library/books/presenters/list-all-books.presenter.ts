// Utils
import { map } from "lodash";
// Presenters
import {
  BookPresenter,
  BookPresenterData,
} from "@bothub/infrastructure/controllers/library/books/presenters/book.presenter";
// Types
import { ListItemsPresenterData } from "@bothub/interfaces";

export interface ListAllBooksPresenterData extends ListItemsPresenterData<BookPresenterData> {}

export class ListAllBooksPresenter implements ListAllBooksPresenterData {
  public rows: BookPresenterData[];
  public total: number;

  constructor(props: ListAllBooksPresenterData) {
    this.rows = map(props.rows, (book) => new BookPresenter(book));
    this.total = props.total;
  }
}
