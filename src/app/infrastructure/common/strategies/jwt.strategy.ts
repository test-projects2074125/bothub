import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
// Utils
import { JwtPayloadData } from "@bothub/core/classes/jwt-payload-data.class";
import { environment } from "@bothub/envs/environment";
// Types
import { JwtPayload } from "@bothub/interfaces";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt-auth") {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: environment.JWT_ACCESS_TOKEN_SECRET,
      ignoreExpiration: false,
      passReqToCallback: false,
    });
  }

  public async validate(payload: JwtPayload): Promise<JwtPayloadData | null> {
    return new JwtPayloadData({
      id: payload.id,
      username: payload.username,
      email: payload.email,
      role: payload.role,
    });
  }
}
