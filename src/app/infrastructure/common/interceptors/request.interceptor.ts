import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  SetMetadata,
  applyDecorators,
  RequestTimeoutException,
} from "@nestjs/common";
import { catchError, Observable, timeout, TimeoutError } from "rxjs";
import { Reflector } from "@nestjs/core";
// Utils
import { GlobalConstants } from "@bothub/core/constants/global.constants";

const SetTimeout = (timeout: number) => SetMetadata("request-timeout", timeout);

export function SetRequestTimeout(timeout = GlobalConstants.DEFAULT_REQUEST_TIMEOUT_MS) {
  return applyDecorators(SetTimeout(timeout));
}

@Injectable()
export class RequestInterceptor<T> implements NestInterceptor<T> {
  constructor(private readonly reflector: Reflector) {}

  public intercept(context: ExecutionContext, next: CallHandler): Observable<void> {
    const requestTimeout: number =
      this.reflector.get("request-timeout", context.getHandler()) ||
      GlobalConstants.DEFAULT_REQUEST_TIMEOUT_MS;

    return next.handle().pipe(
      timeout(requestTimeout),
      catchError((err) => {
        if (err instanceof TimeoutError) {
          throw new RequestTimeoutException();
        }
        throw err;
      }),
    );
  }
}
