import * as process from "node:process";
import * as dotenv from "dotenv";
import * as appRootPath from "app-root-path";
// Utils
import { prodEnv } from "./environment.prod";
import { AppEnvironment, Environment } from "@bothub/types/environment.interface";

// Load environment variables
dotenv.config({
  path: appRootPath.resolve(`.${process.env.NODE_ENV}.env`),
});

const devEnv: AppEnvironment = {
  PORT: 3000,
  SQL_DB: {
    DIALECT: "mysql",
    HOST: "localhost",
    PORT: 3306,
    USERNAME: "root",
    PASSWORD: "JWWbqDk~/4*%]LM",
    DATABASE: "bothub_db",
  },
  NODE_ENV: process.env.NODE_ENV,
  HOST: process.env.HOST,
  JWT_ACCESS_TOKEN_SECRET: process.env.JWT_ACCESS_TOKEN_SECRET,
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
  MAIL_HOST: process.env.MAIL_HOST,
  NO_REPLY_EMAIL: process.env.NO_REPLY_EMAIL,
  NO_REPLY_EMAIL_PASSWORD: process.env.NO_REPLY_EMAIL_PASSWORD,
};

export const environment = process.env.NODE_ENV === Environment.Production ? prodEnv : devEnv;
