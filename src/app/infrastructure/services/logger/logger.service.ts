import { Injectable, Logger } from "@nestjs/common";
// Utils
import { environment } from "@bothub/envs/environment";
// Types
import { ILogger } from "@bothub/interfaces";

@Injectable()
export class LoggerService extends Logger implements ILogger {
  constructor(context: string) {
    super(context);
  }

  public override debug(context: string, message: string): void;
  public override debug(message: string): void;
  public override debug(contextOrMessage: string, message?: string) {
    if (environment.NODE_ENV !== "production") {
      super.debug(message ? `[${contextOrMessage}]: ${message}` : contextOrMessage);
    }
  }

  public override log(context: string, message: string): void;
  public override log(message: string): void;
  public override log(contextOrMessage: string, message?: string) {
    super.log(message ? `[${contextOrMessage}]: ${message}` : contextOrMessage);
  }

  public override error(context: string, message: string, trace?: string): void;
  public override error(message: string): void;
  public override error(contextOrMessage: string, message: string | null = null, trace?: string) {
    if (message && trace) {
      super.error(`[${contextOrMessage}]: ${message}`, trace);
    } else if (message) {
      super.error(`[${contextOrMessage}]: ${message}`);
    } else {
      super.error(contextOrMessage);
    }
  }

  public override warn(context: string, message: string): void;
  public override warn(message: string): void;
  public override warn(contextOrMessage: string, message?: string) {
    super.warn(message ? `[${contextOrMessage}]: ${message}` : contextOrMessage);
  }

  public override verbose(context: string, message: string): void;
  public override verbose(message: string): void;
  public override verbose(contextOrMessage: string, message?: string) {
    if (environment.NODE_ENV !== "production") {
      super.verbose(message ? `[${contextOrMessage}]: ${message}` : contextOrMessage);
    }
  }
}
